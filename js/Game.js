const canvas = document.getElementById('pong')
const ctx = canvas.getContext('2d')

class Pad {

    /**
     * Constructeur d'un Pad
     * @param {boolean} isGauche - Le pad est à gauche ou à droite. 
     */
    constructor(isGauche) {
        this.isGauche = isGauche
        this.w = 2
        this.h = 50
        this.y = (canvas.clientHeight / 2) - (this.h / 2)
        this.score = 0
        if(isGauche) {
            this.x = 10
        } else {
            this.x = canvas.clientWidth - 10 - this.w
        }
    }

    dessiner() {
        ctx.fillStyle = 'white'
        ctx.fillRect(this.x, this.y, this.w, this.h)
        ctx.fill()
        // dessin du score
        ctx.fillStyle = 'white'
        ctx.textAlign = 'center'
        if(this.isGauche) {
            ctx.fillText(this.score, (canvas.clientWidth / 2) - 20, 20)
        } else {
            ctx.fillText(this.score, (canvas.clientWidth / 2) + 20, 20)
        }
        ctx.fill()
    }

    seDeplacer(direction) {
        if(direction === 'haut') {
            console.log('h')
            this.y -= 10
        } else if(direction === 'bas'){
            console.log('b')
            this.y += 10
        }
    }
}

class Balle {
    constructor() {
        this.x = canvas.clientWidth / 2
        this.y = canvas.clientHeight / 2
        this.r = 2
        this.v = 1
        this.dx = Math.round( Math.random() * 1) === 0 ? -1 : 1
        this.dy = Math.round( Math.random() * 1) === 0 ? -1 : 1
    }

    dessiner() {
        ctx.fillStyle = 'white'
        ctx.beginPath()
        ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI)
        ctx.closePath()
        ctx.fill()
    }

    seDeplacer() {
        const positionSuivante = { x: this.x + this.dx, y: this.y + this.dy }
        // collision mur haut
        if(positionSuivante.y - this.r < 0) {
            this.y = 0 + this.r
            this.dy = -this.dy
        } else if(positionSuivante.y + this.r > canvas.clientHeight) {
            this.y = canvas.clientHeight - this.r
            this.dy = -this.dy
        }

        // collision p1
        if(this.dx < 0) {
            if(this.x - this.r < pad1.x + pad1.w) {
                if(this.y > pad1.y && this.y < pad1.y + pad1.h) {
                    this.v += 0.1
                    this.x = pad1.x + pad1.w
                    this.dx = -this.dx
                }
            }
        // collision p2
        } else {
            if(this.x + this.r > pad2.x) {
                if(this.y > pad2.y && this.y < pad2.y + pad2.h) {
                    this.v += 0.1 // acceleration
                    this.x = pad2.x // replacement au bord du pad
                    this.dx = -this.dx // changement de direction
                }
            }
        }

        if(positionSuivante.x < 0) {
            pad2.score++
            this.reset()
        } else if( positionSuivante.x > canvas.clientWidth) {
            pad1.score++
            this.reset()
        }

        this.x = Math.round(this.x + (this.dx * this.v))
        this.y = Math.round(this.y + (this.dy * this.v))
    }

    reset() {
        this.x = canvas.clientWidth / 2
        this.y = canvas.clientHeight /2
        this.v = 1
        this.dx = Math.round( Math.random() * 1) === 0 ? -1 : 1
        this.dy = Math.round( Math.random() * 1) === 0 ? -1 : 1
    }

}

function dessinerTerrain() {
    ctx.clearRect(0,0, canvas.clientWidth, canvas.clientHeight)
    ctx.fillStyle = 'black'
    ctx.fillRect(0, 0, canvas.clientWidth, canvas.clientHeight)
    ctx.fill()
    ctx.fillStyle = 'white'
    ctx.fillRect(canvas.clientWidth / 2, 0, 1, canvas.clientHeight)
    ctx.fill()
}

const pad1 = new Pad(true)
const pad2 = new Pad(false)
const balle = new Balle()
let paused = true

function gererPause() {
    document.addEventListener( 'keyup', (e) => {
        if(e.key === 'p' || e.key === 'P') {
            paused = !paused
        }
    })
    if(paused) {
        ctx.font = '20px'
        ctx.textAlign = 'center'
        ctx.fillStyle = 'white'
        ctx.fillText("Pause. 'P' pour reprendre.", canvas.clientWidth / 2, canvas.clientHeight / 2)
        ctx.font = '20px'
        ctx.textAlign = 'center'
        ctx.fillStyle = 'white'
        ctx.fillText("Commandes: Souris./!\\ Smartphone non-compatible.", canvas.clientWidth / 2, canvas.clientHeight / 2 + 25)
        ctx.fill()
    }
}

function gererDeplacement() {
    
    if(!paused) {
        balle.seDeplacer()
        document.addEventListener('mousemove', (e) => {
            if(!paused) {
                const y = e.clientY
                if(y > canvas.clientHeight - pad1.h) {
                    pad1.y = canvas.clientHeight - pad1.h
                } else {
                    pad1.y = y
                }
            }
        })
        pad2.y = balle.y - pad2.h / 2
    }
}

function loop() {
    dessinerTerrain()
    pad1.dessiner()
    pad2.dessiner()
    balle.dessiner()
    gererPause()
    if(!paused) {
        gererDeplacement()
    }
    window.requestAnimationFrame(loop)
}


window.requestAnimationFrame(loop)
